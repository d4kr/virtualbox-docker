FROM ubuntu:16.04
MAINTAINER Davide "davide@heark.com"

# install virtualbox 5.0
RUN apt update && \
    apt install -y apt-transport-https apt-utils wget && \
    echo 'deb http://download.virtualbox.org/virtualbox/debian xenial contrib non-free' >> /etc/apt/sources.list.d/virtualbox.list && \
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add - && \
    apt update && \
    apt install -y virtualbox-5.0 && \
    modprobe vboxdrv

# clean up
RUN apt clean && \
    rm -rf /var/lib/apt/lists/*

# automatically start virtualbox
ENTRYPOINT ["/usr/bin/virtualbox"]
